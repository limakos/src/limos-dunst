#!/bin/bash

notifyCount=$(cat test.txt | wc -l)

if [ "$notifyCount" -eq 10 ]; then
    sed -i '1d' test.txt 
fi

echo "$1,$2,$3,$4" >> test.txt
